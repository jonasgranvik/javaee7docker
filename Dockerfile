FROM jboss/wildfly:14.0.1.Final

MAINTAINER Jonas Granvik <jonas@granvik.com>

ARG WAR_FILE
ADD target/${WAR_FILE} /opt/jboss/wildfly/standalone/deployments/

