javaee7docker
=============

Simple template application for a Java EE 7 project using Wildfly 14 and Docker.

Usage
-----

**Build image**
    
    1. Start "Docker Quickstart terminal"
    2. run 'mvn package' 
    
Sources
-------
Dockerfile-maven plugin: https://github.com/spotify/dockerfile-maven/
Wildfly docker: https://hub.docker.com/r/jboss/wildfly/